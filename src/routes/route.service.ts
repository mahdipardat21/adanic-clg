import { userRouter } from '../controllers/users/users.router';
import { Router, Application } from 'express';
import {RouteEngine} from './router'


export class RouteService {
    private app: Application;
    private router: RouteEngine;

    public constructor(app: Application) {
        this.app = app
        this.router = new RouteEngine()
        this.bindRouters()
    }

    public bindRouters() {
        this.router.registerRouter("/api/v1/users", userRouter);
    }



    public run() {
        this.router.getRouters().forEach((router: Router, route: string) => {
            this.app.use(route, router)
        })
    }
}

