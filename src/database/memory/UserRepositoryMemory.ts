import { UserRepositoryPort } from "../../core/users/index";
import { FindUserQuery } from "../../core/users/queries/find-user.query";
import { ListUserQuery } from "../../core/users/queries/list-user.query";
import { UserEntity } from "../../core/users/user.entity";
import { Email } from "../../core/users/value-objects/email.value-object";
import { NotFoundException } from "../../libs/exceptions/index";
import { UUID } from "../../libs/value-objects/uuid.value-object";

export class UserRepositoryMemory implements UserRepositoryPort {


    private users: UserEntity[] = [];

    async findOne(query: FindUserQuery): Promise<UserEntity> {
        const find = this.users.find(user => user.id.equals(new UUID(query.id)));
        return find;
    }


    async find(query: ListUserQuery): Promise<UserEntity[]> {
        return this.users.slice((query.page === 0 ? 0 : (query.page - 1) * query.limit), (query.page === 0 ? query.limit : query.page * query.limit))
    }


    async save(user: UserEntity): Promise<void> {
        if (await this.exists(user.email.value)) {
            this.users = this.users.map((mapped: UserEntity) => {

                if (mapped.email.equals(user.email)) {
                    return {
                        ...mapped,
                        firstName: user.firstName,
                        lastName: user.lastName,
                        role: user.role,
                        updatedAt: user.updatedAt
                    } as UserEntity;
                }

                return mapped;
            });
        } else {
            this.users.push(user);
        }
    }

    async exists(email: string): Promise<boolean> {
        return this.users.some((user: UserEntity) => user.email.equals(new Email(email)));
    }

    async findOneOrThrow(id: string): Promise<UserEntity> {
        const user = this.users.find((user: UserEntity) => user.id.equals(new UUID(id)));

        if (!user) {
            throw new NotFoundException();
        }

        return user;

    }

    async remove(id: string): Promise<void> {
        this.users = this.users.filter((user: UserEntity) => !user.id.equals(new UUID(id)))
    }

}