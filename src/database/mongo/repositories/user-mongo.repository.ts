import { FindUserQuery } from "../../../core/users/queries/find-user.query";
import { ListUserQuery } from "../../../core/users/queries/list-user.query";
import { UserEntity, UserRepositoryPort } from "../../../core/users/index";
import { UserOrmMapper } from "./user-orm.mapper";
import { UserModel } from "../models/user.model";
import { NotFoundException } from "../../../libs/exceptions";




export class UserMongoRepository implements UserRepositoryPort {
    private readonly ormMapper: UserOrmMapper;

    constructor() {
        this.ormMapper = new UserOrmMapper();
    }

    async save(user: UserEntity): Promise<void> {

        const userOrmProps = this.ormMapper.toOrmProps(user);

        const existedUser = await UserModel.findById(userOrmProps.userId);

        if (!existedUser) {
            const createdUser = new UserModel(userOrmProps);
            await createdUser.save();
        } else {
            existedUser.set({
                firstName: userOrmProps.firstName,
                lastName: userOrmProps.lastName,
                password: userOrmProps.password,
                role: userOrmProps.role
            });

            await existedUser.save();
        }
    }

    async exists(email: string): Promise<boolean> {
        const user = await UserModel.findOne({ email })
        return user ? true : false
    }

    async findOneOrThrow(id: string): Promise<UserEntity> {
        const user = await UserModel.findById(id);

        if (!user) {
            throw new NotFoundException();
        }


        return this.ormMapper.toDomainProps(user)

    }

    async remove(id: string): Promise<void> {
        await UserModel.findByIdAndRemove(id);
    }

    async findOne(query: FindUserQuery): Promise<UserEntity> {
        const user = await UserModel.findById(query.id);

        return this.ormMapper.toDomainProps(user);
    }

    async find(query: ListUserQuery): Promise<UserEntity[]> {
        const users = await UserModel.find();

        return users.map(user => this.ormMapper.toDomainProps(user))
    }

}