import { UUID } from "../../../libs/value-objects/uuid.value-object";
import { UserEntity, Role, UserProps } from "../../../core/users";
import { UserProp } from "../models/user.model";
import { Email } from "../../../core/users/value-objects/email.value-object";





export class UserOrmMapper {

    toOrmProps(entity: UserEntity): UserProp {
        const props = entity.getPropsCopy();

        const ormProps: UserProp = {
            userId: props.id.value,
            firstName: props.firstName,
            lastName: props.lastName,
            email: props.email.value,
            password: props.password,
            role: props.role
        };

        return ormProps;
    }


    toDomainProps(ormEntity: UserProp): UserEntity {
        const id = new UUID(ormEntity.userId);

        const props: UserProps = {
            firstName: ormEntity.firstName,
            lastName: ormEntity.lastName,
            email: new Email(ormEntity.email),
            password: ormEntity.password,
            role: ormEntity.role as Role
        }

        const user = new UserEntity({id, props})

        return user;
    }
}