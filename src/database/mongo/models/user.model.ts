import { Role } from '../../../core/users/index';
import { Schema, model } from 'mongoose';


export interface UserProp {
    userId: string;
    firstName: string;
    lastName: string;
    email: string;
    password: string;
    role: string;
}

const userSchema = new Schema({
    userId: { type: String, required: true, unique: true },
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String, required: true, unique: true },
    password: { type: String, required: true },
    role: { type: String, required: true, default: Role.operator }
});


export const UserModel = model<UserProp>('User', userSchema)