import { ExceptionBase } from '../libs/exceptions/index'
import {
    Request,
    Response,
    NextFunction
} from 'express'
export default function ExceptionHandler(error: Error, req: Request, res: Response, next: NextFunction) {
    if (error instanceof ExceptionBase) {
        // @TODO: check error type and parse error
        return res.send({
            message: error.message,
            status: "mock todo error, not implement this",
            statusCode: 400
        })
    }

    return res.send({
        message: "internal server error",
        statusCode: 500,
        status: "error"
    });
}
