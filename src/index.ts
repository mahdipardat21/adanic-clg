import { config as loadEnvironmentsVars } from 'dotenv'
import { App } from './app'

/*
**  application execute point
*/

(async function () {
loadEnvironmentsVars()
const port: number = process.env.APP_PORT as unknown as number || 3000
const application = new App(port)
application.start()
})()