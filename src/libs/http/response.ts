import { Transformer } from "./transformer";



export class HttpResponse<T, R> {
    readonly code: number;
    readonly message: string = "success";
    readonly data: any;

    constructor(code: number, message: string, data: any, transformer: Transformer<T, R>) {
        this.code = code;
        this.message = message;
        this.data = Array.isArray(data) ? transformer.collection(data) : transformer.transform(data)
    }


    public serialize() {
        return Object.freeze({
            code: this.code,
            message: this.message,
            data: this.data
        })
    }
}