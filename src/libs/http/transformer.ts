



export abstract class Transformer<T, R> {
    collection(t: T[]): R[] {
        return t.map(this.transform)
    };

    abstract transform(t: T): R;
}