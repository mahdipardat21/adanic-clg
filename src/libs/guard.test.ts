import { Guard } from "./guard"

var isEmptyData = [
    {
        message: "should be return true for undefined | null | empty (string, object, array) | empty array parameters value",
        expected: true,
        input: [undefined, null, '', [], {}, [undefined, null, '']]
    },
    {
        message: "should be return false for number | boolean | not empty (array, object, string) value",
        expected: false,
        input: [12, false, true, [1, 2, 3], { test: "ok" }, "firstName"]
    }
]

describe("Guard utils function", () => {

    describe("Gaurd.isEmpty()", () => {
        for (const data of isEmptyData) {
            test(data.message, () => {
                const isArray = Array.isArray(data.input);
                if (isArray) {
                    for (const input of data.input) {
                        expect(Guard.isEmpty(input)).toBe(data.expected)
                    }
                }

            })
        }
    })

})