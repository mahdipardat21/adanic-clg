
export interface SerializedException {
    message: string;
    code: string;
    metadata?: unknown;
}



export abstract class ExceptionBase extends Error {
    constructor(public readonly message: string, public readonly metadata?: unknown) {
        super(message);
        Error.captureStackTrace(this, this.constructor)
    }

    abstract code: string;


    toJSON(): SerializedException {
        return {
            message: this.message,
            code: this.code,
            metadata: this.metadata
        }
    }
}