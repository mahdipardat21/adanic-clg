import { ExceptionBase } from "./exception.base";
import { ExceptionCodes } from "./exception.codes";




export class BadRequestException extends ExceptionBase {
    code: string = ExceptionCodes.badRequest;

}