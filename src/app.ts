import express, { Application } from 'express'
import { RouteService } from './routes/route.service';

import { startMiddlewares } from './middlewares'
import { startMongoose } from './infra/mongoose';


export class App {
    private readonly app: Application;
    private readonly port: number;
    private readonly router: RouteService;
    constructor(port: number) {
        this.app = express()
        this.port = port
        this.router = new RouteService(this.app)
    }

    public start(): void {
        this.app.use(express.json());
        this.router.run()
        startMiddlewares(this.app)
        startMongoose();

        this.app.listen(this.port, () => {
            console.log(`app instance is running on port: ${this.port}`)
        })



    }
}
