import { FindUserQuery } from "../queries/find-user.query";
import { ListUserQuery } from "../queries/list-user.query";
import { UserEntity } from "../user.entity";



export interface UserRepositoryPort {
    save(user: UserEntity): Promise<void>;
    exists(email: string): Promise<boolean>;
    findOneOrThrow(id: string): Promise<UserEntity>;
    remove(id: string): Promise<void>;

    findOne(query: FindUserQuery): Promise<UserEntity>;
    find(query: ListUserQuery): Promise<UserEntity[]>;
}