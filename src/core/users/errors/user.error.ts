import { ExceptionBase } from '../../../libs/exceptions/index';

export class UserAlreadyExistsError extends ExceptionBase {
  static readonly message: 'User already exists';

  public readonly code = 'USER.ALREADY_EXISTS';

  constructor(metadata?: unknown) {
    super(UserAlreadyExistsError.message, metadata);
  }
}


export class UserNotAccessError extends ExceptionBase {
  static readonly message: 'User does not access';

  public readonly code = 'USER.NOT_ACCESS';

  constructor(metadata?: unknown) {
    super(UserNotAccessError.message, metadata);
  }
}