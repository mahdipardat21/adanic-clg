

export class DeleteUserCommand {
    constructor(props: DeleteUserCommand) {
        this.userId = props.userId;
        this.actionerId = props.actionerId;
    }

    readonly actionerId: string;
    readonly userId: string;
}