import { UserNotAccessError } from "../../errors/user.error";
import { UserRepositoryPort } from "../../ports/user-repository.port";
import { Role } from "../../user.entity";
import { DeleteUserCommand } from "./delete-user.command";




export class DeleteUserService {
    constructor(private readonly _repository: UserRepositoryPort) { }

    async execute(command: DeleteUserCommand): Promise<void> {
        const [actioner, user] = await Promise.all([
            this._repository.findOneOrThrow(command.actionerId),
            this._repository.findOneOrThrow(command.userId)
        ]);


        if (!actioner.id.equals(user.id)) {
            if (actioner.role !== Role.admin || user.role === Role.admin) {
                throw new UserNotAccessError();
            }
        }

        await this._repository.remove(user.id.value);
    }
}