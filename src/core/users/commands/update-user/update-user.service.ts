import { UserNotAccessError } from "../../errors/user.error";
import { UserRepositoryPort } from "../../ports/user-repository.port";
import { Role } from "../../user.entity";
import { UpdateUserCommand } from "./update-user.command";

export class UpdateUserService {
    constructor(private _repository: UserRepositoryPort) { }

    async execute(command: UpdateUserCommand): Promise<void> {
        const [actioner, user] = await Promise.all([
            this._repository.findOneOrThrow(command.actionerId),
            this._repository.findOneOrThrow(command.userId)
        ]);

        if (!actioner.id.equals(user.id) && actioner.role === Role.admin) {
            throw new UserNotAccessError();
        }

        user.updateUser({
            firstName: command.firstName,
            lastName: command.lastName,
        });

        if (actioner.role === Role.admin && command.role !== user.role) {
            command.role === Role.admin ? user.makeAdmin() : user.makeOperator()
        }


        await this._repository.save(user);

    }
}