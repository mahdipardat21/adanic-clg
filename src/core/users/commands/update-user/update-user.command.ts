import { Role } from "../../user.entity";




export class UpdateUserCommand {
    constructor(props: UpdateUserCommand) {
        this.userId = props.userId;
        this.actionerId = props.actionerId;
        this.firstName = props.firstName;
        this.lastName = props.lastName;
        this.role = props.role;
    }

    readonly userId: string;
    readonly actionerId: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly role: Role;
    
}