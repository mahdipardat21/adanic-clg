


export class CreateUserCommand {
    constructor(props: CreateUserCommand) {
        this.email = props.email;
        this.password = props.password;
    }

    readonly email: string;
    readonly password: string;
}