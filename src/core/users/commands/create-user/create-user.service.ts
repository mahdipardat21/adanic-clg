import { UserAlreadyExistsError } from "../../errors/user.error";
import { UserRepositoryPort } from "../../ports/user-repository.port";
import { UserEntity } from "../../user.entity";
import { Email } from "../../value-objects/email.value-object";
import { CreateUserCommand } from "./create-user.command";





export class CreateUserService {
    constructor(private readonly _repository: UserRepositoryPort) {}

    async execute(command: CreateUserCommand): Promise<UserEntity> {
        if (await this._repository.exists(command.email)) {
            throw new UserAlreadyExistsError();
        }

        // @TODO: hash password

        const user = UserEntity.create({
            email: new Email(command.email),
            password: command.password
        })

        await this._repository.save(user);

        return user;
    }
}