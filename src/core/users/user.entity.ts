import { Entity } from "../../libs/base-classes/entity.base";
import { UUID } from "../../libs/value-objects/uuid.value-object";
import { Email } from "./value-objects/email.value-object";

export interface CreateUserProps {
    email: Email;
    password: string;
}

export interface UpdateUserInfoProps {
    firstName: string;
    lastName: string;
}

export interface UserProps extends CreateUserProps, UpdateUserInfoProps {
    role: Role;
}

export enum Role {
    admin = 'admin',
    operator = 'operator'
}

export class UserEntity extends Entity<UserProps> {
    protected readonly _id!: UUID;

    static create(create: CreateUserProps): UserEntity {
        const id = UUID.generate();

        const props: UserProps = { ...create, role: Role.operator, firstName: "", lastName: "" };

        const user = new UserEntity({ id, props });

        return user;
    }

    get firstName(): string {
        return this.props.firstName;
    }

    get lastName(): string {
        return this.props.lastName;
    }

    get email(): Email {
        return this.props.email;
    }

    get role(): Role {
        return this.props.role;
    }

    get password(): string {
        return this.props.password;
    }

    updateUser(update: UpdateUserInfoProps): void {
        this.props.firstName = update.firstName;
        this.props.lastName = update.lastName;
    }

    makeAdmin(): void {
        this.props.role = Role.admin;
    }

    makeOperator(): void {
        this.props.role = Role.operator;
    }

    public validate(): void {

    }

}