import { UserRepositoryPort } from "../index";
import { UserEntity } from "../user.entity";
import { FindUserQuery } from "./find-user.query";




export class FindUserService {
    constructor(private readonly _repository: UserRepositoryPort) { }

    query(query: FindUserQuery): Promise<UserEntity> {
        return this._repository.findOne(query);
    }
}