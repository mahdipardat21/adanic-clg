


export class ListUserQuery {

    constructor(props: ListUserQuery) {
        this.limit = props.limit;
        this.page = props.page;
    }

    readonly limit: number;
    readonly page: number;
}