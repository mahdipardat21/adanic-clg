import { UserRepositoryPort } from "../index";
import { UserEntity } from "../user.entity";
import { ListUserQuery } from "./list-user.query";




export class ListUserService {
    constructor(private readonly _repository: UserRepositoryPort) { }

    query(query: ListUserQuery): Promise<UserEntity[]> {
        return this._repository.find(query);
    }
}