



export class FindUserQuery {

    constructor(props: FindUserQuery) {
        this.id = props.id;
    }

    readonly id: string;
}