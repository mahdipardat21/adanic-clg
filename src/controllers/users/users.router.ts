import { UserRepositoryMemory } from '../../database/memory/UserRepositoryMemory';
import { validationMiddleware } from '../../middlewares/validation-body.middleware';
import { authMiddleware } from '../../middlewares/auth.middleware';
import { Router } from 'express'
import { CreateUserDto } from './create-user.dto';
import { UpdateUserDto } from './update-user.dto';
import { UserController } from './users.controller';
import { UserMongoRepository } from '../../database/mongo/repositories/user-mongo.repository';
const router: Router = Router();

const memRepo = new UserRepositoryMemory();
const mongoRepo = new UserMongoRepository();
const controller = new UserController(mongoRepo);

router.post("/", authMiddleware, validationMiddleware(CreateUserDto), controller.createUser.bind(controller));
router.put("/:id", authMiddleware, validationMiddleware(UpdateUserDto, true), controller.updateUser.bind(controller));
router.delete("/:id", authMiddleware, controller.deleteUser.bind(controller))
router.get("/", authMiddleware, controller.listUsers.bind(controller));
router.get("/:id", authMiddleware, controller.findUser.bind(controller));

export { router as userRouter };