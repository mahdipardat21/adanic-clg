
import { Role } from '../../core/users';
import {IsString, IsEnum} from 'class-validator';


export class UpdateUserDto {
    @IsString()
    firstName: string;

    @IsString()
    lastName: string;

    @IsEnum(Role)
    role: Role;
}