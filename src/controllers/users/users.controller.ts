import { CreateUserCommand, CreateUserService, DeleteUserCommand, DeleteUserService, UpdateUserCommand, UpdateUserService, UserRepositoryPort } from "../../core/users/index";
import { Request, Response } from "express";
import { CreateUserDto } from "./create-user.dto";
import { UpdateUserDto } from "./update-user.dto";
import { FindUserService } from "../../core/users/queries/find-user.service";
import { ListUserService } from "../../core/users/queries/list-user.service";
import { FindUserQuery } from "../../core/users/queries/find-user.query";
import { ListUserQuery } from "../../core/users/queries/list-user.query";
import { HttpResponse } from "../../libs/http/response";
import { UserResponseTransformer } from "./user.response";


export class UserController {
    private readonly createUserService: CreateUserService;
    private readonly updateUserService: UpdateUserService;
    private readonly deleteUserService: DeleteUserService;

    private readonly findUserService: FindUserService;
    private readonly listUserService: ListUserService;

    constructor(userRepository: UserRepositoryPort) {
        this.createUserService = new CreateUserService(userRepository);
        this.updateUserService = new UpdateUserService(userRepository);
        this.deleteUserService = new DeleteUserService(userRepository);
        this.findUserService = new FindUserService(userRepository);
        this.listUserService = new ListUserService(userRepository);
    }

    async createUser(req: Request, res: Response) {
        const body: CreateUserDto = req.body;
        const command = new CreateUserCommand({ email: body.email, password: body.password });
        await this.createUserService.execute(command);
    }

    async deleteUser(req: Request, res: Response) {
        const command = new DeleteUserCommand({ actionerId: '1', userId: req.params.id });
        await this.deleteUserService.execute(command);
    }


    async updateUser(req: Request, res: Response) {
        const body: UpdateUserDto = req.body;
        const command = new UpdateUserCommand({ actionerId: '1', userId: req.params.id, firstName: body.firstName, lastName: body.lastName, role: body.role })

        await this.updateUserService.execute(command)
    }


    async findUser(req: Request, res: Response) {
        const query = new FindUserQuery({ id: req.params.id });

        const user = await this.findUserService.query(query);

        const response = new HttpResponse(200, "success", user, new UserResponseTransformer)

        res.status(response.code).send(response.serialize());
    }


    async listUsers(req: Request, res: Response) {
        const query = new ListUserQuery({ limit: req.query.limit as unknown as number, page: req.query.page as unknown as number });

        const users = await this.listUserService.query(query);

        const response = new HttpResponse(200, "success", users, new UserResponseTransformer);

        res.status(response.code).send(response.serialize())

    }




}