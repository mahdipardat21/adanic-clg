import { UserEntity } from "../../core/users/user.entity";
import { Transformer } from "../../libs/http/transformer";



export interface UserResponse {
    id: string;
    firstName: string;
    lastName: string;
    email: string;
    role: string;
}


export class UserResponseTransformer extends Transformer<UserEntity, UserResponse> {
    transform(t: UserEntity): UserResponse {
        return {
            id: t.id.value,
            firstName: t.firstName,
            lastName: t.lastName,
            email: t.email.value,
            role: t.role
        }
    }

}